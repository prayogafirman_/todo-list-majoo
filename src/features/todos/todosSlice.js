import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchTodos = createAsyncThunk("todos/fetchTodos", async () => {
  const response = await fetch("https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list");
  const todos = await response.json();
  todos.sort(function(a,b){
    // Turn your strings into dates, and then subtract them
    // to get a value that is either negative, positive, or zero.
    return new Date(a.createdAt) - new Date(b.createdAt);
  });
  return todos;
});

const todosSlice = createSlice({
  name: "todos",
  initialState: {
    entities: [],
    loading: false,
  },
  reducers: {
    todoAdded(state, action) {
      state.entities.push(action.payload);
    },
    todoUpdated(state, action) {
      const { id, title, description, createdAt } = action.payload;
      const existingTodo = state.entities.find((todo) => todo.id === id);
      if (existingTodo) {
        existingTodo.title = title;
        existingTodo.description = description;
        existingTodo.createdAt = createdAt;
      }
    },
    todoDeleted(state, action) {
      const { id } = action.payload;
      const existingTodo = state.entities.find((todo) => todo.id === id);
      if (existingTodo) {
        state.entities = state.entities.filter((todo) => todo.id !== id);
      }
    },
    todoUpdateStatus(state, action) {
      const { id } = action.payload;
      const { status } = action.payload;
      const existingTodo = state.entities.find((todo) => todo.id === id);
      if (existingTodo) {
        existingTodo.status = status;
      }
    },
  },
  extraReducers: {
    [fetchTodos.pending]: (state, action) => {
      state.loading = true;
    },
    [fetchTodos.fulfilled]: (state, action) => {
      state.loading = false;
      state.entities = [...state.entities, ...action.payload];
    },
    [fetchTodos.rejected]: (state, action) => {
      state.loading = false;
    },
  },
});

export const { todoAdded, todoUpdated, todoDeleted, todoUpdateStatus } = todosSlice.actions;

export default todosSlice.reducer;