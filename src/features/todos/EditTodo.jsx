import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";

import { useState } from "react";
import { todoUpdated } from "./todosSlice";

export function EditTodo() {
  const { pathname } = useLocation();
  const todoId = parseInt(pathname.replace("/edit-todo/", ""));

  const todo = useSelector((state) =>
    state.todos.entities.find((todo) => todo.id === todoId)
  );

  const dispatch = useDispatch();
  const history = useHistory();

  const [name, setName] = useState(todo.name);
  const [email, setEmail] = useState(todo.email);
  const [error, setError] = useState(null);

  const handleName = (e) => setName(e.target.value);
  const handleEmail = (e) => setEmail(e.target.value);

  const handleClick = () => {
    if (name && email) {
      dispatch(
        todoUpdated({
          id: todoId,
          name,
          email,
        })
      );

      setError(null);
      history.push("/");
    } else {
      setError("Fill in all fields");
    }
  };

  return (
    <div className="container">
      <div className="row">
        <h1>Edit todo</h1>
      </div>
      <div className="row">
        <div className="three columns">
          <label htmlFor="nameInput">Name</label>
          <input
            className="u-full-width"
            type="text"
            placeholder="test@mailbox.com"
            id="nameInput"
            onChange={handleName}
            value={name}
          />
          <label htmlFor="emailInput">Email</label>
          <input
            className="u-full-width"
            type="email"
            placeholder="test@mailbox.com"
            id="emailInput"
            onChange={handleEmail}
            value={email}
          />
          {error && error}
          <button onClick={handleClick} className="button-primary">
            Save todo
          </button>
        </div>
      </div>
    </div>
  );
}
