import { useDispatch, useSelector } from "react-redux";

import { useHistory } from "react-router-dom";
import { useState } from "react";
import { todoAdded } from "./todosSlice";

export function AddTodo() {
  const dispatch = useDispatch();
  const history = useHistory();

  const [name, setTitle] = useState("");
  const [email, setDescription] = useState("");
  const [error, setError] = useState(null);

  const handleName = (e) => setTitle(e.target.value);
  const handleEmail = (e) => setDescription(e.target.value);

  const todosAmount = useSelector((state) => state.todos.entities.length);

  const handleClick = () => {
    if (name && email) {
      dispatch(
        todoAdded({
          id: todosAmount + 1,
          name,
          email,
        })
      );

      setError(null);
      history.push("/");
    } else {
      setError("Fill in all fields");
    }

    setTitle("");
    setDescription("");
  };

  return (
    <div className="container">
      <div className="row">
        <h1>Add todo</h1>
      </div>
      <div className="row">
        <div className="three columns">
          <label htmlFor="nameInput">Name</label>
          <input
            className="u-full-width"
            type="text"
            placeholder="test@mailbox.com"
            id="nameInput"
            onChange={handleName}
            value={name}
          />
          <label htmlFor="emailInput">Email</label>
          <input
            className="u-full-width"
            type="email"
            placeholder="test@mailbox.com"
            id="emailInput"
            onChange={handleEmail}
            value={email}
          />
          {error && error}
          <button onClick={handleClick} className="button-primary">
            Add todo
          </button>
        </div>
      </div>
    </div>
  );
}
