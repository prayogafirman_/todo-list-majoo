import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import { TodoList } from "./features/todos/TodoList";
import { AddTodo } from "./features/todos/AddTodo";
import { EditTodo } from "./features/todos/EditTodo";
import React from "react";
import "./App.css";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/add-todo">
            <AddTodo />
          </Route>
          <Route path="/edit-todo">
            <EditTodo />
          </Route>
          <Route path="/">
            <TodoList />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
