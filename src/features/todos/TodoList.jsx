import { fetchTodos,todoDeleted, todoUpdated, todoUpdateStatus, todoAdded} from "./todosSlice";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";

import { useHistory } from "react-router-dom";
import '../../App.css';


export function TodoList() {
  const dispatch = useDispatch();

  const { entities } = useSelector((state) => state.todos);
  const [isShowModal, setisShowModal] = useState(false);
  const [isShowModalUpdate, setisShowModalUpdate] = useState(false);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [createdAt, setcreatedAt] = useState("");

  const [titleUpdate, setTitleUpdate] = useState("");
  const [descriptionUpdate, setDescriptionUpdate] = useState("");
  const [createdAtUpdate, setcreatedAtUpdate] = useState("");

  const [error, setError] = useState(null);
  const [currentData, setcurrentData] = useState(null);
  const history = useHistory();

  const handleDelete = (id) => {
    dispatch(todoDeleted({ id }));
    setTitle("");
    setDescription("");
    setcreatedAt("");
    setError(null);
    setisShowModalUpdate(false);
  };

  const handleName = (e) => setTitle(e.target.value);
  const handleEmail = (e) => setDescription(e.target.value);
  const handlecreatedAt = (e) => setcreatedAt(e.target.value);

  const handleNameUpdate = (e) => setTitleUpdate(e.target.value);
  const handleEmailUpdate = (e) => setDescriptionUpdate(e.target.value);
  const handlecreatedAtUpdate = (e) => setcreatedAtUpdate(e.target.value);

  const handleClick = () => {
    if (title && description && createdAt) {
      let status = 0;
      dispatch(
        todoAdded({
          id: todosAmount + 1,
          title,
          description,
          status,
          createdAt,
        })
      );
      setTitle("");
      setDescription("");
      setcreatedAt("");
      setError(null);
      setisShowModal(false);
      history.push("/");
    } else {
      setError("Fill in all fields");
    }
  };

  const updateData = () => {
    if (titleUpdate && descriptionUpdate && createdAtUpdate) {
      let id = currentData.id;
      let title = titleUpdate
      let description = descriptionUpdate
      let createdAt = createdAtUpdate
      dispatch(
        todoUpdated({
          id,
          title,
          description,
          createdAt,
        })
      );
      setTitle("");
      setDescription("");
      setcreatedAt("");
      setError(null);
      setisShowModalUpdate(false);
      fetchTodos()
      history.push("/");
    } else {
      setError("Fill in all fields");
    }
  };

  const changeStatus = (id,status) => {
    dispatch(todoUpdateStatus({ id,status }));
  }

  const todosAmount = useSelector((state) => state.todos.entities.length);

  return (
    <div className="container">
      <div className="row">
        <h1>Simple Todo List</h1>
      </div>
      <div className="row mb-4">
        <div className="two columns">
          <button className="button-primary" onClick={()=>{
              setisShowModal(true)
          }}>Add todo</button>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-6 p-0 m-0">
          <div className="card mr-1 ">
            <div className="card-body">
              <h4 className="card-title">Undone Todo List</h4>
              <div className="list-group">
              {entities.length &&
                entities.map(({ id, title, description, status,createdAt}, i) => (
                <div key={i}>
                  {status === 0 &&
                    <div className="list-group-item bg-light todo-list shadow mb-2 rounded-lg" style={{cursor:'pointer'}}>
                      <div className="row">
                        <div className="col-sm-10" onClick={()=>{
                          setisShowModalUpdate(true);
                          setcurrentData(entities[i]);
                          setTitleUpdate(entities[i].title);
                          setDescriptionUpdate(entities[i].description);
                          setcreatedAtUpdate(entities[i].createdAt);
                        }}>
                          <h4 className="list-group-item-heading">{title}</h4>
                          <p className="list-group-item-text">{description}</p>
                          <small>{createdAt}</small>
                        </div>
                        <div className="col-sm-2">
                          <button type="button" onClick={()=>{
                              changeStatus(id,1)
                          }} className="btn mt-4 btn-sm btn-info">Done</button>
                        </div>
                      </div>
                    </div>
                  }
                </div>
              ))}
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-6 p-0 m-0">
          <div className="card ml-1 ">
            <div className="card-body">
              <h4 className="card-title">Done Todo List</h4>
              {entities.length &&
                entities.map(({ id, title, description, status,createdAt}, i) => (
                <div key={i}>
                  {status === 1 &&
                    <div className="list-group-item bg-light todo-list shadow mb-2 rounded-lg" style={{cursor:'pointer'}}>
                      <div className="row">
                        <div className="col-sm-10" onClick={()=>{
                          setisShowModalUpdate(true);
                          setcurrentData(entities[i]);
                          setTitleUpdate(entities[i].title);
                          setDescriptionUpdate(entities[i].description);
                          setcreatedAtUpdate(entities[i].createdAt);
                        }}>
                          <h4 className="list-group-item-heading">{title}</h4>
                          <p className="list-group-item-text">{description}</p>
                          <small>{createdAt}</small>
                        </div>
                        <div className="col-sm-2">
                          <button type="button" onClick={()=>{
                              changeStatus(id,0)
                          }} className="btn mt-4 btn-sm btn-warning">Undone</button>
                        </div>
                      </div>
                    </div>
                  }
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div>
        {isShowModal === true &&
          <div id="exampleModalLive" className="modal fade show" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style={{display:'block',background:'#0000009e'}} onClick={(e)=>{
            setisShowModal(false)
          }}>
            <div className="modal-dialog" role="document" onClick={(e)=>{
                  e.stopPropagation();
            }}>
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLiveLabel">Add New Todo</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={()=>{
                      setisShowModal(false)
                  }}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                    <label htmlFor="titleInput">Title</label>
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Title..."
                      id="titleInput"
                      onChange={handleName}
                      value={title}
                    />
                  </div>
                  <label htmlFor="descriptionInput">Description</label>
                  <input
                    className="u-full-width"
                    type="text"
                    placeholder="Description..."
                    id="descriptionInput"
                    onChange={handleEmail}
                    value={description}
                  />
                  <div className="form-group">
                    <label htmlFor="createdAtInput">Date</label>
                    <input
                      className="form-control"
                      type="datetime-local"
                      placeholder="Date...."
                      style={{height:'40px'}}
                      id="createdAtInput"
                      onChange={handlecreatedAt}
                      value={createdAt}
                    />
                  </div>
                  <span className="text-danger">{error && error}</span>
                </div>
                <div className="modal-footer">
                  <button type="button" onClick={()=>{
                        setisShowModal(false)
                    }} className="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" onClick={handleClick} className="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
        }

        {isShowModalUpdate === true &&
          <div id="update" className="modal fade show" tabIndex="-1" role="dialog" aria-labelledby="updateLabel" style={{display:'block',background:'#0000009e'}} onClick={()=>{
                setisShowModalUpdate(false)
            }}>
            <div className="modal-dialog" role="document" onClick={(e)=>{
                  e.stopPropagation();
            }}>
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="updateLabel">Update Todo</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={()=>{
                      setisShowModalUpdate(false)
                  }}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                    <label htmlFor="titleInputUpdate">Title</label>
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Title..."
                      id="titleInputUpdate"
                      onChange={handleNameUpdate}
                      value={titleUpdate}
                    />
                  </div>
                  <label htmlFor="descriptionInputUpdate">Description</label>
                  <input
                    className="u-full-width"
                    type="text"
                    placeholder="Description..."
                    id="descriptionInputUpdate"
                    onChange={handleEmailUpdate}
                    value={descriptionUpdate}
                  />
                  <div className="form-group">
                    <label htmlFor="createdAtInputUpdate">Date</label>
                    <input
                      className="form-control"
                      type="datetime-local"
                      placeholder="Date...."
                      style={{height:'40px'}}
                      id="createdAtInputUpdate"
                      onChange={handlecreatedAtUpdate}
                      value={createdAtUpdate}
                    />
                  </div>
                  <span className="text-danger">{error && error}</span>
                </div>
                <div className="modal-footer">
                  <button type="button" onClick={()=>{
                    const r = window.confirm("Hapus todo?"); 
                    if(r === true){ 
                      handleDelete(currentData.id);
                    } 
                  }} className="btn btn-danger" data-dismiss="modal">Delete Todo</button>
                  <button type="button" onClick={updateData} className="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    </div>
  );
}